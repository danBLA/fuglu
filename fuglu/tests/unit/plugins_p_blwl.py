# -*- coding: UTF-8 -*-
import unittest
from fuglu.plugins.p_blwl import BlockWelcomeEntry, FugluBlockWelcome
from fuglu.shared import Suspect, FuConfigParser


class FugluBlockWelcomeTest(FugluBlockWelcome):
    def __init__(self, config, section, listings):
        super().__init__(config, section)
        self.listing_data = listings
    
    def _get_listings(self, fugluid=''):
        listings = {}
        for item in self.listing_data:
            listing = BlockWelcomeEntry(**item)
            scope = item['scope']
            try:
                listings[scope].append(listing)
            except KeyError:
                listings[scope] = [listing]
        return listings


class FugluBlockWelcomeTestCase(unittest.TestCase):
    
    def setUp(self):
        config = FuConfigParser()
        section = 'BlockWelcome'
        config.add_section(section)
        config.set(section, 'fublwl_eval_order', 'user,domain,global')
        config.set(section, 'fublwl_update_hitcount', 'False')
        config.set(section, 'fublwl_header_checks', 'False')
        self.config = config
        self.section = section
    
    def tearDown(self):
        pass
    
    def test_listing_ip(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': 32, 'scope': '$GLOBAL', 'sender_addr': None, 'sender_host': '8.8.8.8'},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), 'meh')
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '9.9.9.9', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), 'meh')
        
        
    def test_listing_sender_email(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': 'sender@unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), 'meh')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), 'meh')
    
    
    def test_listing_sender_dom(self):
        listings = [
            {'comment': '', 'list_id': 1, 'list_scope': BlockWelcomeEntry.SCOPE_ENV, 'list_type': 'welcome', 'netmask': -1, 'scope': '$GLOBAL', 'sender_addr': '%unittests.fuglu.org', 'sender_host': None},
        ]
        candidate = FugluBlockWelcomeTest(self.config, self.section, listings)
        
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), 'meh')
        
        suspect = Suspect('somebody@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertTrue(suspect.is_welcomelisted(), 'meh')
        
        suspect = Suspect('sender@othertests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        suspect.clientinfo = 'mail.example.com', '8.8.8.8', 'mail.example.com'
        candidate.evaluate(suspect)
        print(suspect.tags)
        self.assertFalse(suspect.is_welcomelisted(), 'meh')

