# -*- coding: UTF-8 -*-
import unittest
from fuglu.plugins.antivirus import SSSPPlugin
from fuglu.shared import FuConfigParser
from unittest.mock import patch
from unittest.mock import MagicMock


class SSSPTestCase(unittest.TestCase):

    def setUp(self):
        config = FuConfigParser()
        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        config.add_section('SSSPPlugin')
        config.set('SSSPPlugin', 'maxsize', 5000000000)
        config.set('SSSPPlugin', 'retries', 1)
        config.set('SSSPPlugin', 'problemaction', 'DEFER')
        # current tests don't need config options, add them here later if
        # necessary
        self.config = config

    def tearDown(self):
        pass
    
    @patch("fuglu.plugins.antivirus.SSSPPlugin._say_goodbye")
    @patch("fuglu.plugins.antivirus.SSSPPlugin._receive_msg")
    @patch("fuglu.plugins.antivirus.SSSPPlugin._accepted")
    @patch("fuglu.plugins.antivirus.SSSPPlugin._exchange_greetings")
    @patch("fuglu.plugins.antivirus.SSSPPlugin._read_options")
    def test_answer(self, rops, exchgr, acc, rcvmsg, sgb):
        """Test parsing of sophos answer, especially removal of tmp-folder in name"""

        rops.return_value = {'maxscandata': ['0'], 'version': ['SAV Dynamic Interface 2.6.0'],
                             'maxclassificationsize': ['4096'],
                             'method': ['QUERY SERVER', 'QUERY SAVI', 'QUERY ENGINE', 'OPTIONS',
                                         'SCANDATA', 'SCANFILE', 'SCANDIR'], 'maxmemorysize': ['250000']}
        acc.return_value = True
        rcvmsg.return_value = \
          [b'EVENT FILE /tmp/savid_tmpgMEMBE', b'FILE /tmp/savid_tmpgMEMBE', b'TYPE D0',
           b'EVENT ARCHIVE /tmp/savid_tmpgMEMBE/AAAA0001', b'FILE /tmp/savid_tmpgMEMBE/AAAA0001',
           b'TYPE D0', b'EVENT ARCHIVE /tmp/savid_tmpgMEMBE/AAAA0001/AAAA0001',
           b'FILE /tmp/savid_tmpgMEMBE/AAAA0001/AAAA0001', b'TYPE 80',
           b'EVENT ARCHIVE /tmp/savid_tmpgMEMBE/AAAA0001/AAAA0002',
           b'FILE /tmp/savid_tmpgMEMBE/AAAA0001/AAAA0002', b'TYPE D9',
           b'EVENT ARCHIVE /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip',
           b'FILE /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip', b'TYPE 30',
           b'EVENT ARCHIVE /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip/AAAAAAAAA%20AA%20AAAAAAAA.exe',
           b'FILE /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip/AAAAAAAAA%20AA%20AAAAAAAA.exe',
           b'TYPE 60', b'TYPE 81', b'TYPE 53', b'TYPE 60', b'TYPE 81',
           b'EVENT VIRUS Mal/DummyFlu /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip/AAAAAAAAA%20AA%20AAAAAAAA.exe',
           b'VIRUS Mal/DummyFlu /tmp/savid_tmpgMEMBE/AAAAAAAAA%20AA%20AAAAAAAA.zip/AAAAAAAAA%20AA%20AAAAAAAA.exe',
           b'OK 0203 /tmp/savid_tmpgMEMBE', b'DONE OK 0203 Virus found during virus scan']

        candidate = SSSPPlugin(self.config)
        candidate.__init_socket__ = MagicMock()
        candidate.__init_socket__.return_value = MagicMock()

        reply = candidate.scan_stream(b"dummy")
        # ideally we don't want the tmp-folder structure in the message
        # /tmp/savid_tmpgMEMBE should be removed by the regex antivirus.tmpdirsyntax
        targetanswer = {'AAAAAAAAA%20AA%20AAAAAAAA.zip/AAAAAAAAA%20AA%20AAAAAAAA.exe': 'Mal/DummyFlu'}
        self.assertEqual(targetanswer, reply)
