# -*- coding: utf-8 -*-
import tempfile
import unittest
import logging
import sys
import multiprocessing
import threading
import time
import os
from unittest import mock
from os import getpid
from mock import patch, MagicMock

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.message import EmailMessage

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from fuglu.plugins.uriextract import URIExtract, EmailExtract, DOMAINMAGIC_AVAILABLE, URIExtractAppender,\
    EmailExtractAppender, DomainAction
from fuglu.shared import Suspect, DUNNO, FuConfigParser, SuspectFilter, _SuspectTemplate

from .unittestsetup import TESTDATADIR



def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


if DOMAINMAGIC_AVAILABLE:
    class URIExtractTest(unittest.TestCase):
        def setUp(self):
            section="URIExtract"

            tlds="com net org\n .co.uk ch ru"
            with open('/tmp/unittest-tld.txt','w') as f:
                f.write(tlds)

            skiplist="skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt','w') as f:
                f.write(skiplist)


            config=FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', '50000000')
            config.set(section, 'maxsize_analyse', '20000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            config.set(section, 'usehacks', "1")
            self.candidate=URIExtract(config,section)
            self.candidate._prepare('unittests.fuglu.org')

        def tearDown(self):
            pass

        def test_timout(self):
            section="URIExtract"

            tlds="com net org\n .co.uk ch ru"
            with open('/tmp/unittest-tld.txt','w') as f:
                f.write(tlds)

            skiplist="skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt','w') as f:
                f.write(skiplist)

            config=FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', '50000000')
            config.set(section, 'maxsize_analyse', '20000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', '0.1')
            candidate = URIExtract(config,section)

            # small trick, prepare manually and then make it a sleep to test the timeout
            candidate._prepare('unittests.fuglu.org')
            candidate._prepare = lambda *args: time.sleep(0.11)
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_base64.eml'))
            candidate.examine(suspect=suspect)
            self.assertTrue(suspect._stimehit.get(section, False))

        def test_simple_text(self):
            txt="""hello http://bla.com please click on <a href="www.co.uk">slashdot.org/?a=c&f=m</a> www.skipme.com www.skipmenot.com/ http://allinsurancematters.net/lurchwont/ muahahaha x.org"""

            uris=self.candidate.extractor.extracturis(txt)
            for want in ['http://bla.com', 'www.co.uk', 'www.skipmenot.com/', "http://allinsurancematters.net/lurchwont/", "x.org"]:
                self.assertTrue(want in uris, f'wanted={want} got={uris}')
            self.assertTrue("skipme.com" not in " ".join(uris))

        def test_dotquad(self):
            txt="""click on 1.2.3.4 or http://62.2.17.61/ or https://8.8.8.8/bla.com """

            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('1.2.3.4' in uris)
            self.assertTrue('http://62.2.17.61/' in uris)
            self.assertTrue('https://8.8.8.8/bla.com' in uris)

        def test_uppercase(self):
            txt="""hello http://BLa.com please click"""
            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('http://bla.com' not in uris,'uris should not be lowercased')
            self.assertTrue('http://BLa.com' in uris,'uri with uppercase not found')

        def test_url_without_file(self):
            txt="""lol http://roasty.familyhealingassist.ru?coil&commission blubb"""
            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('http://roasty.familyhealingassist.ru?coil&commission' in uris,'did not find uri, result was %s'%uris)

        def test_withSuspect_TE(self):
            """Test using suspect, link is in the base64 transfer encoded part"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_base64.eml'))

            logger.debug("examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            logger.debug('uris: '+",".join(uris))

            self.assertTrue('www.co.uk' in uris)

        def test_unquoted_link(self):
            """Test unquoted href attribute"""
            txt="""hello  <a href=www.co.uk>slashdot.org/?a=c&f=m</a> """

            uris=self.candidate.extractor.extracturis(txt)
            self.assertTrue('www.co.uk' in uris)
            self.assertTrue('slashdot.org/?a=c&f=m' in uris)

        def test_withSuspect(self):
            """Test unquoted href attribute in html part of mail"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_html.eml'))

            logger.debug("examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            logger.debug('uris: '+",".join(uris))
            self.assertTrue( "http://toBeDetected.com.br/Jul2018/En/Statement/Invoice-DDDDDDDDD-DDDDDD/" in uris)

        def test_uri_display_invisiblechars(self):
            """Test extraction of uri with invisible characters in html a-tag display name"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(*[TESTDATADIR, 'mail_html_invisible.eml']))

            logger.debug("examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            for uri in uris:
                print(uri)
            logger.debug('uris: '+",".join(uris))
            # a-tag href
            self.assertTrue("http://toBeDetected.com.br/Jul2018/En/Statement/Invoice-DDDDDDDDD-DDDDDD/" in uris)
            # a-tag display
            self.assertTrue("http://tobefound.com/bla/bla" in uris)

        def test_html_base(self):
            """Test if html mail with <base> and <a> tag are combined"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "me@domain.invalid"

            you = "you@domain.invalid"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail with html"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Please open html part..."

            html = """\
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <base href="https://fuglu.org/">
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="a/b/?email=daemon@fuglu.org">link</a> you wanted.<br>       
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html', _charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")

            suspect.set_source(msg.as_bytes())

            print("Examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            #print(uris)
            self.assertIn("https://fuglu.org/a/b/?email=daemon@fuglu.org", uris)
        
        
        def test_html_nomulti(self):
            """try to extract content of html only withouth multipart designation and with broken utf8"""
            tempfile = os.path.join(TESTDATADIR, "uriextract-html-nomulti.eml")

            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tempfile)

            # no exception is fine for this case
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            #print(uris)
            self.assertIn('https://fakepharmacy.fuglu.org/apotheke.html', uris)
        
        
        def test_plaintext_wsdotws(self):
            """Test if html mail with <base> and <a> tag are combined"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "me@domain.invalid"

            you = "you@domain.invalid"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = EmailMessage()
            msg['Subject'] = "mail with html"
            msg['From'] = me
            msg['To'] = you
            msg.set_payload('bla blubb www . example . net foo bar')
            
            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")

            suspect.set_source(msg.as_bytes())

            print("Examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            #print(uris)
            self.assertIn("www.example.net", uris)



    class URIExtractTestSpecialconfigs(unittest.TestCase):

        def setUp(self):
            tlds = "com net org\n .co.uk ch ru"
            open('/tmp/tld1.txt', 'w').write(tlds)

            skiplist = "skipme.com meetoo.com"
            open('/tmp/domainskiplist1.txt', 'w').write(skiplist)

        def tearDown(self):
            pass

        def test_largemail(self):
            """Test a large mail"""

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'maxsize', '100')
            config.set(section, 'maxsize_analyse', '1000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            path_filename = os.path.abspath(__file__)
            [path, _] = os.path.split(path_filename)
            testdatadir = os.path.normpath(os.path.join(*[path, "testdata"]))
            filename = os.path.join(testdatadir, '6mbzipattachment.eml')

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)

            print("Mail size: %u, URIExtract maxsize: %u, URIExtract maxsize_analyse: %u"
                  % (suspect.size,
                     candidate.config.getint(candidate.section, 'maxsize'),
                     candidate.config.getint(candidate.section, 'maxsize_analyse')
                     )
                  )

            # test is useless if message is too small
            self.assertLess(candidate.config.getint(candidate.section, 'maxsize'), suspect.size)

            print("examine suspect")
            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            if uris:
                print('uris: '+", ".join(uris))
            else:
                print('no uris found')

            self.assertTrue('https://www.fuglu.org' in uris)

        def test_largemail_analyselimit(self):
            """Test a mail large mail, second part should be skipped due to analyse limit"""

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'maxsize', '100')
            config.set(section, 'maxsize_analyse', '33')
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            path_filename = os.path.abspath(__file__)
            [path, _] = os.path.split(path_filename)
            testdatadir = os.path.normpath(os.path.join(*[path, "testdata"]))
            filename = os.path.join(testdatadir, '6mbzipattachment.eml')

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)

            print("Mail size: %u, URIExtract maxsize: %u, URIExtract maxsize_analyse: %u"
                  % (suspect.size,
                     candidate.config.getint(candidate.section, 'maxsize'),
                     candidate.config.getint(candidate.section, 'maxsize_analyse')
                     )
                  )

            # test is useless if message is too small
            self.assertLess(candidate.config.getint(candidate.section, 'maxsize'), suspect.size)

            print("examine suspect")
            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            if uris:
                print('uris: '+", ".join(uris))
            else:
                print('no uris found')

            self.assertTrue('https://www.fuglu.org' in uris)
            self.assertEqual(1, len(uris), "Uri from second text part should be ignored")

    class URIExtractAppenderTest(unittest.TestCase):
        def setUp(self):
            section = "URIExtractAppender"

            tlds = "com net org co.uk ch ru"
            with open('/tmp/unittest-tld.txt', 'w') as f:
                f.write(tlds)

            skiplist="skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt','w') as f:
                f.write(skiplist)

            config=FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', '10485000')
            config.set(section, 'maxsize_analyse', '2000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'with_envelope_sender', 'False')
            config.set(section, 'timeout', "0")
            self.candidate = URIExtractAppender(config, section)
            self.candidate._prepare('unittests.fuglu.org')

        def tearDown(self):
            pass

        def test_withSuspect(self):
            """Test using suspect, link is in the base64 transfer encoded part"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_base64.eml'))

            logger.debug("examine suspect")
            self.candidate.process(suspect, DUNNO)

            uris = suspect.get_tag('body.uris')
            logger.debug('uris: '+",".join(uris))

            self.assertTrue('www.co.uk' in uris)


    class EmailExtractTest(unittest.TestCase):
        """Test email address extraction"""

        def setUp(self):
            section = "EmailExtract"

            tlds = "com net org\n .co.uk ch ru"
            with open('/tmp/unittest-tld.txt', 'w') as f:
                f.write(tlds)

            skiplist = "skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt', 'w') as f:
                f.write(skiplist)


            config=FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', '10485000')
            config.set(section, 'maxsize_analyse', '2000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'with_envelope_sender', 'False')
            config.set(section, 'headers', 'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
            config.set(section, 'skipheaders', 'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')
            config.set(section, 'timeout', "0")

            self.candidate=EmailExtract(config,section)
            self.candidate._prepare('unittests.fuglu.org')

        def test_withSuspect(self):
            """Test email address extraction"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "my@tobefound.com"
            address2befound.append(me)

            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail address"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Hi!\nHow are you?\nHere is the link you wanted:\n" \
                   "https://www.python.org\nAnd here's the address:\n%s."%addr

            addr = "webmaster@findmeinhtml.com"
            html = """\
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="https://www.python.org">link</a> you wanted.<br>       
                   And here's the <a href="mailto:%s"> mail address</a>.<br>
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """%addr
            address2befound.append(addr)

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html',_charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            headerlist = ['Return-Path', 'Reply-To', 'From', 'X-RocketYMMF', 'X-Original-Sender', 'Sender',
                          'X-Originating-Email', 'Envelope-From', 'Disposition-Notification-To']
            for hdr in headerlist:
                addr = hdr.lower()+"@tobefound.com"
                msg[hdr] = addr
                address2befound.append(addr)
                print("New/changed header to be found %s: %s"%(hdr,msg[hdr]))

            skipheaderlist = ['X-Original-To', 'Delivered-To', 'X-Delivered-To,' 'Apparently-To', 'X-Apparently-To']
            for hdr in skipheaderlist:
                msg[hdr] = hdr.lower()+"@tobeskipped.com"
                print("New/changed header to be skipped %s: %s"%(hdr,msg[hdr]))

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org',"/dev/null")

            try:
                suspect.set_source(msg.as_bytes())
            except AttributeError:
                suspect.set_source(msg.as_string())

            print("Examine suspect")
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))

            missing = []
            for addr in address2befound:
                if addr in emailaddresses:
                    print("Found: %s"%addr)
                else:
                    print("DID NOT FIND: %s"%addr)
                    missing.append(addr)

            over = []
            for addr in emailaddresses:
                if addr not in address2befound:
                    print("DID FIND ADDRESS WHICH SHOULD NOT BE FOUND: %s"%addr)
                    over.append(addr)

            self.assertEqual(0,len(missing),"Not all mail addresses detected! Missing:\n[%s]\n\n"%", ".join(missing))
            self.assertEqual(0,len(over),"Found addresses that should be skipped! List is:\n[%s]\n\n"%", ".join(over))

        def test_withSuspect_Header_problem(self):
            """From bugfix where EmailExtract complained about a getting a Header instead of str in Py3"""
            myclass = self.__class__.__name__
            functionNameAsString = sys._getframe().f_code.co_name
            loggername = "%s.%s" % (myclass,functionNameAsString)
            logger = logging.getLogger(loggername)

            logger.debug("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'mail_html.eml'))
            msg = suspect.get_message_rep()
            msg['Reply-To'] = "rec@aaaaaaa.aa"                   # works
            msg['Reply-To'] = Header("rec@bbbbbbb.bb").encode()  # works
            msg['Reply-To'] = Header("rec@ccccccc.cc")           # (failing before bugfix)
            msg['Reply-To'] = Header("rec@ddddddd.dd")           # works
            suspect.set_message_rep(msg)

            # no exception is fine for this case
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))
            # rec@aaaaaaa.aa and rec@ddddddd.dd are ignored because ".aa" and ".dd" are no valid tlds
            self.assertEqual(sorted(["rec@bbbbbbb.bb", "rec@ccccccc.cc"]), sorted(emailaddresses))


        def test_header_problem(self):
            """From bugfix where EmailExtract complained about a getting a Header instead of str in Py3"""
            tempfile = os.path.join(TESTDATADIR, "emailextracterror.eml")

            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tempfile)

            # no exception is fine for this case
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))
            self.assertEqual((["bbbbbbb@bbbbbbbbb.bb"]), sorted(emailaddresses))

        def test_domainextraction(self):
            """Test email address domain extraction"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "my@tobefound.com"
            address2befound.append(me)

            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail address"
            msg['From'] = me
            msg['To'] = you
            msg['Reply-To'] = "rec@findmeintext.com"

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Hi!\nHow are you?\nHere is the link you wanted:\n" \
                   "https://www.python.org\nAnd here's the address:\n%s."%addr

            addr = "webmaster@findmeinhtml.com"
            html = r"""\
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="https://www.python.org">link</a> you wanted.<br>       
                   And here's the <a href="mailto:%s"> mail address</a>.<br>
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """%addr
            address2befound.append(addr)

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html',_charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org',"/dev/null")

            suspect.set_source(msg.as_bytes())

            print("Examine suspect")
            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            emailaddresses_hdr = suspect.get_tag('header.emails')
            emailaddresses_bdy = suspect.get_tag('body.emails')

            print('email(all) addresses found: '+",".join(emailaddresses))
            print('email(hdr) addresses found: '+",".join(emailaddresses_hdr))
            print('email(bdy) addresses found: '+",".join(emailaddresses_bdy))

            # check emails
            self.assertEqual(sorted(["my@tobefound.com",
                                     "rec@findmeintext.com"]),
                             sorted(emailaddresses_hdr), "Not all header email addresses found")

            self.assertEqual(sorted([ "webmaster@findmeintext.com",
                                     "webmaster@findmeinhtml.com"]),
                             sorted(emailaddresses_bdy), "Not all body email addresses found")

            self.assertEqual(sorted(["my@tobefound.com",
                                     "webmaster@findmeintext.com",
                                     "rec@findmeintext.com",
                                     "webmaster@findmeinhtml.com"]),
                             sorted(emailaddresses), "Not all email addresses found")
            # check domains
            emaildomains = suspect.get_tag('emails.domains')
            emaildomains_hdr = suspect.get_tag('header.emails.domains')
            emaildomains_bdy = suspect.get_tag('body.emails.domains')

            print('email(all) domains found: '+",".join(emaildomains))
            print('email(hdr) domains found: '+",".join(emaildomains_hdr))
            print('email(bdy) domains found: '+",".join(emaildomains_bdy))

            # check email domains
            self.assertEqual(sorted(["tobefound.com",
                                     "findmeintext.com"]),
                             sorted(emaildomains_hdr), "Not all header email domains found")

            self.assertEqual(sorted([ "findmeintext.com",
                                      "findmeinhtml.com"]),
                             sorted(emaildomains_bdy), "Not all body email domains found")

            self.assertEqual(sorted(["tobefound.com",
                                     "findmeintext.com",
                                     "findmeinhtml.com"]),
                             sorted(emaildomains), "Not all email domains found")

    class EmailExtractTestOnlyHref(unittest.TestCase):
        """Test email address extraction, only href extraction part"""

        candidate = None
        tldtxt = None
        skiplisttxt = None

        @classmethod
        def setUpClass(cls) -> None:
            section = "EmailExtract"

            tlds = "com net org\n .co.uk ch ru"
            cls.tldtxt = tempfile.NamedTemporaryFile(suffix='email-extract', prefix='fuglu-unittest', dir='/tmp')
            cls.tldtxt.write(tlds.encode())
            cls.tldtxt.seek(0)


            skiplist = "skipme.com meetoo.com"
            cls.skiplisttxt = tempfile.NamedTemporaryFile(suffix='skiplist', prefix='fuglu-unittest', dir='/tmp')
            cls.skiplisttxt.write(skiplist.encode())
            cls.skiplisttxt.seek(0)

            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', cls.tldtxt.name)
            config.set(section, 'domainskiplist', cls.skiplisttxt.name)
            config.set(section, 'maxsize', '10485000')
            config.set(section, 'maxsize_analyse', '2000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'with_envelope_sender', 'False')
            config.set(section, 'headers',
                       'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
            config.set(section, 'skipheaders',
                       'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')
            config.set(section, 'timeout', "0")

            cls.candidate = EmailExtract(config, section)
            cls.candidate._prepare('unittests.fuglu.org')

            # patch object to disable basic extractor
            cls.candidate.extractor.extractemails = mock.MagicMock()
            # whenever called, return a NEW empty list
            # return_value would return the SAME list which could be modified outside...
            cls.candidate.extractor.extractemails.side_effect = lambda x: list()

        def test_href_brackets_mail(self):
            """square brackets should be ignored"""

            me = "my@tobefound.com"
            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "test"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            text = "Html only in this text :-)"

            html = """
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="[mailto:aaaa@aa-aaaaaaa.com]">mail</a> you wanted.<br>       
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html', _charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            suspect = Suspect(me, you, "/dev/null")
            suspect.set_source(msg.as_bytes())

            self.candidate.examine(suspect)

            emailaddresses = suspect.get_tag('emails')
            print('emails: '+", ".join(emailaddresses))
            self.assertEqual([], emailaddresses)


    class EmailExtractAppenderTest(unittest.TestCase):
        """Test email address extraction"""

        def setUp(self):
            section = "EmailExtractAppender"

            tlds = "com net org\n .co.uk ch ru"
            with open('/tmp/unittest-tld.txt', 'w') as f:
                f.write(tlds)

            skiplist = "skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt', 'w') as f:
                f.write(skiplist)

            config=FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
            config.set(section, 'maxsize', '10485000')
            config.set(section, 'maxsize_analyse', '2000000')
            config.set(section, 'loguris', 'no')
            config.set(section, 'with_envelope_sender', 'False')
            config.set(section, 'headers', 'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
            config.set(section, 'skipheaders', 'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')
            config.set(section, 'timeout', "0")

            self.candidate = EmailExtractAppender(config, section)
            self.candidate._prepare('unittests.fuglu.org')

        def test_withSuspect(self):
            """Test email address extraction"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "my@tobefound.com"
            address2befound.append(me)

            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail address"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Hi!\nHow are you?\nHere is the link you wanted:\n" \
                   "https://www.python.org\nAnd here's the address:\n%s."%addr

            addr = "webmaster@findmeinhtml.com"
            html = r"""\
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="https://www.python.org">link</a> you wanted.<br>       
                   And here's the <a href="mailto:%s"> mail address</a>.<br>
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """%addr
            address2befound.append(addr)

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html',_charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            headerlist = ['Return-Path', 'Reply-To', 'From', 'X-RocketYMMF', 'X-Original-Sender', 'Sender',
                          'X-Originating-Email', 'Envelope-From', 'Disposition-Notification-To']
            for hdr in headerlist:
                addr = hdr.lower()+"@tobefound.com"
                msg[hdr] = addr
                address2befound.append(addr)
                print("New/changed header to be found %s: %s"%(hdr,msg[hdr]))

            skipheaderlist = ['X-Original-To', 'Delivered-To', 'X-Delivered-To,' 'Apparently-To', 'X-Apparently-To']
            for hdr in skipheaderlist:
                msg[hdr] = hdr.lower()+"@tobeskipped.com"
                print("New/changed header to be skipped %s: %s"%(hdr,msg[hdr]))

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org',"/dev/null")

            try:
                suspect.set_source(msg.as_bytes())
            except AttributeError:
                suspect.set_source(msg.as_string())

            print("Examine suspect")
            self.candidate.process(suspect, DUNNO)

            emailaddresses = suspect.get_tag('emails')
            print('email addresses found: '+",".join(emailaddresses))

            missing = []
            for addr in address2befound:
                if addr in emailaddresses:
                    print("Found: %s"%addr)
                else:
                    print("DID NOT FIND: %s"%addr)
                    missing.append(addr)

            over = []
            for addr in emailaddresses:
                if addr not in address2befound:
                    print("DID FIND ADDRESS WHICH SHOULD NOT BE FOUND: %s"%addr)
                    over.append(addr)

            self.assertEqual(0, len(missing),
                             "Not all mail addresses detected! Missing:\n[%s]\n\n" % ", ".join(missing))
            self.assertEqual(0, len(over),
                             "Found addresses that should be skipped! List is:\n[%s]\n\n" % ", ".join(over))

        def test_href_cleanup(self):
            """href mail cleanup tests"""

            # for testing, collect all addresses that should be found
            address2befound = []

            # me == my email address
            # you == recipient's email address
            me = "me@domain.invalid"

            you = "you@domain.invalid"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "mail with html"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            addr = "webmaster@findmeintext.com"
            address2befound.append(addr)
            text = "Please open html part..."

            html = """\
            <html>                                                                                           
              <head>Explanations in comment</head>                                                                                  
              <body>                                                                                         
                <a href="mailto:  mail1@findmeintext.com">don't create separate mail hit due to space</a> 
                <a href="mailto::mail2@findmeintext.com">don't create separate mail hit due to double colon</a> 
                <a href="mailto:mail3@findmeintext.com?subject=[aaaa]">don't create separate mail hit due to subject parameter</a> 
                <a href="mailto:mail4@dontfindmeintext .com">invalid</a> 
                <a href="mailto:bli bla blubb mail5@findmeintext.com">extract only valid mail part</a> 
                <a href="mailto:whatever@mail5@findmeintext.com">invalid</a> 
              </body>                                                                                        
            </html>                                                                                          
            """

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html', _charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            print("Create suspect")
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")

            suspect.set_source(msg.as_bytes())

            print("Examine suspect")
            self.candidate.examine(suspect)

            uris = suspect.get_tag('body.emails')
            print(uris)
            self.assertEqual(
                sorted(['mail1@findmeintext.com', 'mail2@findmeintext.com', 'mail3@findmeintext.com',
                        'mail5@findmeintext.com']),
                sorted(uris))


    class URIExtractMultiproc(unittest.TestCase):
        """Some problems have been found in multiprocessing mode using the domainmagic module
        that's why it gets here a separate test"""
        def setUp(self):

            tlds="com net org\n .co.uk ch ru"
            with open('/tmp/unittest-tld.txt','w') as f:
                f.write(tlds)

            skiplist="skipme.com meetoo.com"
            with open('/tmp/domainskiplist.txt', 'w') as f:
                f.write(skiplist)

        def test_parallelrun(self):
            """Tests domainmagic for its multiprocessing capabilities which is required uding URIExtract
            in fuglu with multiprocessing backend"""

            import domainmagic

            # check version of domainmagic
            dversion = [int(i) for i in domainmagic.__version__.split(".")]
            dminversion = [0, 0, 10]
            newer = False
            equal = True
            for dv, dmv in zip(dversion, dminversion):
                newer = newer or (dv > dmv)
                equal = equal and (dv == dmv)

            if not (newer or equal):
                print("\n")
                print("-------------------------------------------------------------")
                print("-- This test can only run with domainmagic 0.0.10 or newer --")
                print("-------------------------------------------------------------")
                print("Version detected is: %s (%s)" % (domainmagic.__version__,".".join([str(i) for i in dversion])))
                print("\n")
                return

            #from domainmagic.fileupdate import fileupdater  <- won't work, will remain None
            import domainmagic.fileupdate


            root = logging.getLogger()
            root.setLevel(logging.ERROR)


            if domainmagic.fileupdate.fileupdater is not None:
                domainmagic.fileupdate.fileupdater.is_recent = mock.Mock(return_value=False)
            else:
                print("%u: no fileupdater object has been created yet" % getpid())

            parallel_tasks = 2
            resultqueue = multiprocessing.Queue()
            jobs = []

            # run worker once before creating processes
            run_worker(None, 0, -1)

            # get the id of the Fileupdater
            fileupdaterid = domainmagic.fileupdate.fileupdater.id()

            print("------------------")
            for i in range(parallel_tasks):
                workerid = i+1  # workerid starts with 1 so it can be distinguished from main thread which has 0
                p = multiprocessing.Process(target=run_worker, args=(resultqueue, workerid, fileupdaterid))
                jobs.append(p)

            for p in jobs:
                p.start()

            njobs = len(jobs)
            for p,i in zip(jobs,range(njobs)):
                # max timeout for the first job
                p.join(timeout=(njobs-i))
                print("Process %u is alive: %s" % (i, p.is_alive()))

            resultqueue.put(None)
            errorprocs = []
            while True:
                procanswer = resultqueue.get(1)
                if procanswer is None:
                    # this is the end of the queue
                    break
                pid, status = procanswer
                print("process %u, success %u" % (pid, status))
                if status != 0:
                    errorprocs.append(pid)

            self.assertEqual(0, len(errorprocs))

    def run_worker(resultqueue, workerid, fileupdaterid):

        #from domainmagic.fileupdate import fileupdater  <- won't work, will remain None, global var "fileupdater" has
        #                                                   to be accessed as domainmagic.fileupdate.fileupdater

        import domainmagic.fileupdate

        # a mock to force file update
        is_recent_mock = mock.Mock(return_value=False)

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (beginning of run_worker) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (beginning of run_worker) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (beginning of run_worker) no fileupdater object has been created yet" % getpid())

        if workerid == 0:
            # In the beginning when the main thread reaches here
            # there should be no fileupdater yet
            if (fileupdaterid != -1):
                raise ValueError("Main process should be called only once with hardcoded '-1' as id")
            # Unfortunately during tests it can not be guaranteed another test using domainmagic
            # has run before and therefore domainmagic.fileupdate.fileupdater might not be None anymore

        section="EmailExtract"
        config=FuConfigParser()
        config.add_section(section)
        config.set(section, 'tldfiles', "/tmp/unittest-tld.txt")
        config.set(section, 'domainskiplist', "/tmp/domainskiplist.txt")
        config.set(section, 'maxsize', '10485000')
        config.set(section, 'maxsize_analyse', '2000000')
        config.set(section, 'loguris', 'no')
        config.set(section, 'with_envelope_sender', 'False')
        config.set(section, 'headers', 'Return-Path,Reply-To,From,X-RocketYMMF,X-Original-Sender,Sender,X-Originating-Email,Envelope-From,Disposition-Notification-To')
        config.set(section, 'skipheaders', 'X-Original-To,Delivered-To,X-Delivered-To,Apparently-To,X-Apparently-To')
        config.set(section, 'timeout', "0")

        candidate=URIExtract(config,section)
        candidate._prepare('unittests.fuglu.org')
        candidate.extractor.maxregexage = 0

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (after preparing URIExtract) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (after preparing URIExtract) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (after preparing URIExtract) no fileupdater object has been created yet" % getpid())

        if workerid != 0 and fileupdaterid == domainmagic.fileupdate.fileupdater.id():
            raise ValueError("Subprocesses should have a different fileupdeter-object (and therefore id)\n"
                             "than the main thread after using it.")

        # Keep playing with a local threading.Lock here
        # In the old implementation it was getting stuck trying to
        # acquire a lock (randomly happened on some os version only)
        lock = threading.Lock()

        for i in range(2):
            lock.acquire()
            time.sleep(0.01)
            lock.release()
            txt="""hello http://bla.com please click on <a href="www.co.uk">slashdot.org/?a=c&f=m</a> www.skipme.com www.skipmenot.com/ http://allinsurancematters.net/lurchwont/ muahahaha x.org"""
            uris=candidate.extractor.extracturis(txt)

        if domainmagic.fileupdate.fileupdater is not None:
            if domainmagic.fileupdate.fileupdater.is_recent is not is_recent_mock:
                domainmagic.fileupdate.fileupdater.is_recent = is_recent_mock
            print("%u: (after extracting uris) fileupdate id: %u, instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), id(domainmagic.fileupdate.FileUpdaterMultiproc.instance)))
            print("%u: (after extracting uris) fileupdate id: %u, access -> instance id: %u" %
                  (getpid(), id(domainmagic.fileupdate), domainmagic.fileupdate.fileupdater.id()))
        else:
            print("%u: (after extracting uris) no fileupdater object has been created yet" % getpid())

        procid = getpid()
        target_found_list = ['http://bla.com', 'www.co.uk', 'slashdot.org/?a=c&f=m']
        for targeturi, i in zip(target_found_list,range(len(target_found_list))):
            if targeturi not in uris:
                if resultqueue is not None:
                    resultqueue.put((procid, i+1))
                raise ValueError("process %u -> %s not found!" % (procid, targeturi))

        if resultqueue is not None:
            resultqueue.put((procid, 0))


    class URIExtractTestHref(unittest.TestCase):
        """Test using href in atags"""
        
        def _gen_html_mail(self, url, me="my@tobefound.com", you="your@tobeskipped.com"):
            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "test"
            msg['From'] = me
            msg['To'] = you
    
            # Create the body of the message (a plain-text and an HTML version).
            text = "Html only in this text :-)"
    
            html = f"""
                    <html>
                      <head></head>
                      <body>
                        <p>Hi!<br>
                           How are you?<br>
                           Here is the <a href="{url}">link</a> you wanted.<br>
                        </p>
                      </body>
                    </html>
                    """
    
            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html', _charset="UTF-8")
    
            msg.attach(part1)
            msg.attach(part2)
            return msg
            

        def test_href_space(self):
            """Test email address extraction"""
            msg = self._gen_html_mail("https://aaaaaaa.aaaaaaaaaa.com/aaaaaaaaaaa.aaaaaaa.com/da aa aa aa/aaaddaa#aaaa@aa-aaaaaaa.com")

            suspect = Suspect(msg['From'], msg['To'], "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertIn("https://aaaaaaa.aaaaaaaaaa.com/aaaaaaaaaaa.aaaaaaa.com/da aa aa aa/"
                          "aaaddaa#aaaa@aa-aaaaaaa.com", uris)


        def test_href_spaceencoded(self):
            """Test email address extraction"""
            msg = self._gen_html_mail("https://aaaaaaa.aaaaaaaaaa.com/aaaaaaaaaaa.aaaaaaa.com/da%20aa%20aa%20aa/aaaddaa#aaaa@aa-aaaaaaa.com")

            suspect = Suspect(msg['From'], msg['To'], "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertIn("https://aaaaaaa.aaaaaaaaaa.com/aaaaaaaaaaa.aaaaaaa.com/da%20aa%20aa%20aa/"
                          "aaaddaa#aaaa@aa-aaaaaaa.com", uris)

        def test_href_broken_space(self):
            """Test email address extraction"""
            msg = self._gen_html_mail("http s://tw.b id.yahoo.com/")

            suspect = Suspect(msg['From'], msg['To'], "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertIn("https://tw.bid.yahoo.com/", uris)
        
        
        def test_ignore_mail_cid(self):
            """Test if email address/dicd are ignored in href"""
            msg = self._gen_html_mail("mailto:aaaa@aa-aaaaaaa.com")

            suspect = Suspect(msg['From'], msg['To'], "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual([], uris)


        def test_ignore_templ(self):
            """Test if templates (in square brackets) are ignored"""
            msg = self._gen_html_mail("[LINK]")

            suspect = Suspect(msg['From'], msg['To'], "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual([], uris)


        def test_quickfilter(self):
            """Test quick filter and fixups"""
            
            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            candidate = URIExtract(config, section)
            
            uris = [
                #(want, have),
                ('https://file1.example.com/Mailing/2018/l_arrw01.gif', 'https:///https://file1.example.com/Mailing/2018/l_arrw01.gif'),
                ('https://file1.example.com/Mailing/2018/l_arrw01.gif', 'https://https:///file1.example.com/Mailing/2018/l_arrw01.gif'),
                ('https://example.com/', 'https://example.com /'),
                ('https://example.com/', 'blob:https://example.com/'),
                ('https://example.com/', '=3D"https://example.com/'),
                ('', 'https://www.example.or.xn--kr)-7r5v14v/'),
                #('', 'http://pdf2019autoedited/PdfDrive2019/l0gin.htm'),
                ('', 'tel:123456789'),
                ('', '#foobar'),
                ('https://127.0.0.1/index.html', 'https://127.0.0.1/index.html'),
                ('https://[::1]/index.html', 'https://[::1]/index.html'),
                ('https://210.16.101.132/index.html', 'https://0322.0020.0145.0204/index.html'),
            ]
            
            for want, have in uris:
                result = candidate._quickfilter([have])
                if want:
                    self.assertIn(want, result, f'wanted {want} got {result}')
                else:
                    self.assertEqual([], result, f'wanted nothing got {result}')
        


    class URIExtractSubject(unittest.TestCase):
        """Test using href in atags"""

        def test_uri_in_subject(self):
            """Test email address extraction"""

            me = "my@tobefound.com"
            you = "your@tobeskipped.com"

            # Create message container - the correct MIME type is multipart/alternative.
            msg = MIMEMultipart('alternative')
            msg['Subject'] = "test https://aaaaaaa.tobefound1.com"
            msg['From'] = me
            msg['To'] = you

            # Create the body of the message (a plain-text and an HTML version).
            text = "Html only in this text :-)"

            html = """
            <html>                                                                                           
              <head></head>                                                                                  
              <body>                                                                                         
                <p>Hi!<br>                                                                                   
                   How are you?<br>                                                                          
                   Here is the <a href="https://aaaaaa.tobefound2.com">link</a> you wanted.<br>       
                </p>                                                                                         
              </body>                                                                                        
            </html>                                                                                          
            """

            # Record the MIME types of both parts - text/plain and text/html.
            part1 = MIMEText(text, 'plain')
            part2 = MIMEText(html, 'html', _charset="UTF-8")

            msg.attach(part1)
            msg.attach(part2)

            suspect = Suspect(me, you, "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual(sorted(["https://aaaaaa.tobefound2.com",
                              "https://aaaaaaa.tobefound1.com"]),
                             sorted(uris))

    class URIExtractNewlineHTML(unittest.TestCase):
        """Test URI extraction in html part after newline"""

        def test_uri_newline(self):
            """Test URI in html after newline"""
            me = "my@tobefound.com"
            you = "your@tobeskipped.com"

            html = """
<html>                                                                                           
<head></head>                                                                                  
<body>                                                                                         
<p>Hi!<br>                                                                                   
bla bla bla.
https://aaaaaaa.tobefound1.com
Here is the <a href="https://aaaaaa.tobefound2.com">link</a> you wanted.<br>       
</p>                                                                                         
</body>                                                                                        
</html>                                                                                          
"""

            # Create message container - the correct MIME type is multipart/alternative.
            #msg = MIMEMultipart('alternative')
            msg = MIMEText(html, 'html', _charset="UTF-8")
            msg['Subject'] = "test"
            msg['From'] = me
            msg['To'] = you


            suspect = Suspect(me, you, "/dev/null")
            suspect.set_source(msg.as_bytes())

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'usehacks', 'yes')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual(sorted(["https://aaaaaa.tobefound2.com",
                                     "https://aaaaaaa.tobefound1.com"]),
                             sorted(uris))


    class URIExtractMRelated(unittest.TestCase):
        """Tests with multipart/related mimetype, especially with broken headers..."""

        def test_correct_mail(self):
            """Extract links from a mail which has correct mime structure for multipart related"""
            me = "my@tobefound.com"
            you = "your@tobeskipped.com"

            suspect = Suspect(me, you, os.path.join(*[TESTDATADIR, "mrelated.eml"]))
            print(suspect.att_mgr.get_fileslist())

            self.assertEqual(3, len(suspect.att_mgr.get_fileslist()),
                             f"There should be 3 mail parts detected,"
                             f"but found {len(suspect.att_mgr.get_fileslist())}:"
                             f"{suspect.att_mgr.get_fileslist()}")

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'usehacks', 'true')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual(sorted(["https://aaaaaa.tobefound2.com",
                                     "https://aaaaaaa.tobefound1.com",
                                     "https://tools.ietf.org/html/rfc2387#section-5.2"]),
                             sorted(uris))

        def test_broken_mail(self):
            """Broken header -> handle content as text if buffer has been detected as text"""
            me = "my@tobefound.com"
            you = "your@tobeskipped.com"

            suspect = Suspect(me, you, os.path.join(*[TESTDATADIR, "mrelated_broken.eml"]))

            print(suspect.att_mgr.get_fileslist())
            self.assertEqual(1, len(suspect.att_mgr.get_fileslist()),
                             f"There should be 3 mail parts detected,"
                             f"but found {len(suspect.att_mgr.get_fileslist())}:"
                             f"{suspect.att_mgr.get_fileslist()}")

            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'usehacks', 'true')
            config.set(section, 'timeout', "0")
            candidate = URIExtract(config, section)
            candidate._prepare('unittests.fuglu.org')

            candidate.examine(suspect)

            uris = suspect.get_tag('body.uris')
            print('uris: '+",".join(uris))
            self.assertEqual(sorted(["https://aaaaaa.tobefound2.com",
                                     "https://aaaaaaa.tobefound1.com",
                                     "https://tools.ietf.org/html/rfc2387#section-5.2"]),
                             sorted(uris))

    class URIExtractHeaders(unittest.TestCase):
        def setUp(self):
            section = "URIExtract"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'tldfiles', "/tmp/tld1.txt")
            config.set(section, 'domainskiplist', "/tmp/domainskiplist1.txt")
            config.set(section, 'loguris', 'no')
            config.set(section, 'usehacks', 'true')
            config.set(section, 'uricheckheaders', 'from, to, reply-to')
            config.set(section, 'timeout', "0")
            self.candidate = URIExtract(config, section)
            self.candidate._prepare('unittests.fuglu.org')

        def test_simplehit(self):
            filename = os.path.join(TESTDATADIR, 'helloworld.eml')
            self.assertTrue(os.path.exists(filename))
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
            msgrep = suspect.get_message_rep()

            link2befound = "https://aaaaaaa.tobefound1.com"
            msgrep["To"] = f"\"{link2befound}\" <recipient@domain.invalid>"

            suspect.set_message_rep(msgrep=msgrep)
            self.candidate.examine(suspect=suspect)
            headeruris = suspect.get_tag('headers.uris', [])
            print(headeruris)
            self.assertIn(link2befound, headeruris)

        def test_encoded(self):
            filename = os.path.join(TESTDATADIR, 'helloworld.eml')
            self.assertTrue(os.path.exists(filename))
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
            msgrep = suspect.get_message_rep()

            link2befound = "https://aaaaaaa.tobefound1.com"

            # create encoded header, follow strategy from Suspect.prepend_header_to_source
            u_value = f"\"üüü {link2befound} äääää\" <recipient@domain.invalid>"
            # is ignore the right thing to do here?
            # (moved from add_header) routine
            b_value = u_value.encode('UTF-8', 'ignore')
            hdr = Header(b_value, charset='utf-8', header_name="To", continuation_ws=' ')
            hdrstring = hdr.encode()
            print(f"encoded hdrstring: {hdrstring}")

            self.assertIn("utf-8", hdrstring, "Header value should be utf-8 encoded!")

            # now add header
            msgrep["To"] = hdrstring

            suspect.set_message_rep(msgrep=msgrep)
            self.candidate.examine(suspect=suspect)
            headeruris = suspect.get_tag('headers.uris', [])
            print(headeruris)
            self.assertIn(link2befound, headeruris)


    class TestDomainAction(unittest.TestCase):
        """DomainAction tests"""

        def setUp(self):
            section = "DomainAction"
            config = FuConfigParser()
            config.add_section(section)
            config.set(section, 'blacklistconfig', '${confdir}/rbl.conf')
            config.set(section, 'checksubdomains', 'yes')
            config.set(section, 'action', 'DUNNO')
            config.set(section, 'message', '5.7.1 black listed URL ${domain} by ${blacklist}',)
            config.set(section, 'maxlookups', '10')
            config.set(section, 'randomise', 'False')
            config.set(section, 'check_all', 'False')
            config.set(section, 'extra_tld_file', '')
            config.set(section, 'testentry', '')
            config.set(section, 'exceptions_file', '')
            config.set(section, 'suspect_tags', 'body.uris',)
            config.set(section, 'userpref_types', 'uridnsbl_skip_domain')
            config.set(section, 'userpref_dbconnection', '')
            config.set(section, 'userpref_usecache', "True")
            # store config for later use
            self.section = section
            self.config = config

        def test_ignore_invalid(self):
            """Test invalid link is ignored"""
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
            suspect.set_tag('body.uris', ["http://[CLICK_AAAAAAA/clicks/img/img1.jpg|http://domain.invalid/AAAA/img/imag100]"])

            candidate = DomainAction(self.config, self.section)

            candidate.rbllookup = MagicMock()
            candidate.examine(suspect=suspect)
            self.assertFalse(candidate.rbllookup.called)
