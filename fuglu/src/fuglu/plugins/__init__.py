# -*- coding: UTF-8 -*-
#   Copyright Oli Schacher, Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
__all__ = [
    'a_logging', 'a_statsd', 'antivirus', 'archive', 'attachment', 'bacn', 'bacn', 'call_ahead', 'clamav',
    'dataprovider', 'decision', 'delay', 'dnsdata', 'domainauth', 'fuzor', 'geoip',
    'knownsubject', 'mailcopy', 'messagesize', 'originpolicy', 'outpolicy', 'p_blwl', 'p_debug', 'p_fraction',
    'p_skipper', 'restrictions', 'rspamd', 'sa', 'script', 'tlspolicy', 'uriextract', 'vacation',
]
