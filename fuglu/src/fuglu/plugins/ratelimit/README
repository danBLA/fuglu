This plugin is based on the ratelimit plugin in the postomaat project (https://gitlab.com/fumail/postomaat)
developed by @ledgr

This is a generic rate limiting plugin. It allows limiting the amount of accepted events
based on any combination of supported SuspectFilter fields. This means you could for example
limit the number of similar subjects by sender domain to implement a simple bulk filter.

Important notes:
    - This plugin is experimental and has not been tested in production
    - This plugin only makes sense in pre-queue mode.
    - The content filter stage is usually *not* the best place to implement rate-limiting.
        Faster options are postfix built-in rate limits or a policy access daemon
        which doesn't need to accept the full message to make a decision
    - the backends don't automatically perform global expiration of all events.
        Old entries are only cleared per event the next time the same event happens.
        Add a cron job for your backend to clear all old events from time to time.

Supported backends:
    - memory: stores events in memory. Do not use this in production.
    - sqlalchemy: Stores events in a SQL database. Recommended for small/low-traffic setups
    - redis: stores events in a redis database. This is the fastest and recommended backend.

Configuration example for redis:
    backendtype = redis
    backendconfig = redis://:[password]@127.0.0.1:6379/0
Prerequisites:
    - python redis module


Configuration example for mysql:
    backendtype = sqlalchemy
    backendconfig = mysql://root@localhost/postomaat
Prerequisites:
    - python sqlalchemy module.
    - the database must exist.
    - the table will be created automatically.


ratelimit.yml format: (not final yet)

Each limiter is defined by a unique name in the yml file. The following parameters are available:

<name>: # A descriptive name for this filter, one word. Required to reference in skip lists
    priority: <number> # an integer number to prioritise rules (lower number = higher priority, default = 0)
    strategy: <strategy> # A strategy to use for this ratelimit, e.g. 'fixed' or 'rolling-window'
    key: <keyname> # Optional a keyname for the limiter (used for the counter) to overwrite default value <name>
    rate: # rate can be defined with number & frame or directly as string "rate: x/y"
            number: <max> #  The maximum number of events that may occur in the specified timeframe before an action is limited.
            frame: <timeframe> # Timeframe for the limit in seconds
    mstate: # can be defined directly as string "mstate: state1, state2, state3" or as an array
        - <state1> # milter states: connect, helo, mailfrom, rcpt, header, eoh, eob
        - <state2> # or normal plugin state: eom
    count:
        <fieldname>: # several fieldnames are possible
            type: <fieldtype> # "Suspect" for Suspect attributes,
                              # "SuspectTag" for getting tags of suspect
                              # "MilterSession" for attributes of MilterSession (milter state only)
            field: <fieldname> # custom or dependent field calculation, works the same way as 'key'
                               # but is applied before regex/ipmatch is applied and therefore thes
                               # matches will be applied on the modified field
                - <func> ${from_address}     # use a template variable
                - <func> module1.function1
            regex: # regex can be defined directly as string "regex: ^.*@domain.invalid$" ar as list
                - <reg1> # Regular expression to apply to the actual values. The limiter is only applied if this regular expression matches.
                - <reg2>
            key: <keyname> # define a common key for counter (otherwise field value is used)
                           # or function as "<func> module.functionname" or as an array of functions
                           # which is applied one after the other where the output is passed as argument
                           # Note <keyname> can contain template vars, for example "${heloname}"
                           # If an array is given, results are passed to the next function...
                - <func> ${from_address}     # use a template variable
                - <func> module1.function1
                - <func> module2.function2(myfactor=(int)5) # brackets can be used to pass additional keyword args
                                                            # type (int/float/bool) can be given to define type
                - <func> module2.function3[1][0] # square brackets can be used to select args from the function output
                - <func> module2.function4(myfactor=5,passarg=) # keyword arg without value means to pass this
                                                                  arg from previous call
            ipmatch: # can be defined directly as string "ipmatch: 192.168.1.0/24"
                - <net1> # define network (for example 192.168.0.0/16)
                - <net2> # another network
            lint: # array of tests for lint, needing an input or templatedict and an examine result
                - input: <fieldinput for lint>           # patch the return value of the field evaluation
                  templatedict:                          # for more complex templates which need several inputs
                      heloname: 192.168.0.1.fuglu.org    # from suspect dict, e.g. ${heloname}${to_address}
                      to_address: unknown@domain
                  examine: True
                - input: <another input>
                  examine: False
    action: <action> # Action that should be performed if the limit is exceeded. ( REJECT / DEFER / ... )
    message: <message> # Message returned to the connecting client (can contains templates like ${from_address}
    sum: <method> # how to count, can be event/recipients/data
                  #   event -> count once per call (for example recipient state counting messages)
                  #   recipients -> add number or recipients per call (for example end-of-data state counting messages)
                  #   data -> sum up data to limit data rate (size * number of recipients)
                  #   zero -> just check if limiter is already over the limit
                  #   size -> sum up data to limit data rate (message size)


---------
Examples:
---------

```
# max 100 messages per hour per server helo
serverhelo:
    strategy: rolling-window
    rate: 100/3600
    message: Bulk message detected
    action: REJECT
    count:
        clienthelo:
            type: Suspect

# max 60 messages per 60 seconds per unique sender address (controlling the bursts):
fromaddr_r:
    strategy: rolling-window
    rate:
        number: 60
        frame: 60
    message: ${from_address} is sending too fast
    action: REJECT
    count:
        from_address:
            type: Suspect

# max 2000 messages per 24 hours with a cooldown time of 24 hours from the last
# sent message (controlling the total allowed amount of emails). Consider only
# senders from domain "domain.invalid" and use one counter for this domain
fromaddr_f:
    strategy: fixed
    rate:
        number: 2000
        frame: 86400
    message: many messages from ${from_address}
    action: REJECT
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
            key: domain.invalid

# Create a whitelist entry
# General restriction for senders from "*@domain.invalid". Due to the 'key' attribute
# restriction is on the whole domain. The whitelist rule is applied before due to priority
# and will always hit (due to strategy) if sender is "*@domain.invalid" and recipient
# "allowed@domain.invalid". For strategy "always-hit" the 'rate' parameter is neglected.

whitelist:
    priority: 1
    strategy: always-hit
    rate: 1/1
    message: ${{from_address}} to ${{to_address}} has an exception
    action: DUNNO
    count:
        to_address:
            type: Suspect
            regex: ^allowed\@domain\.invalid$
        from_address:
            type: Suspect
            regex: ^.*\@domain\.invalid$
general_restriction:
    priority: 2
    strategy: fixed
    rate:
        number: 2000
        frame: 86400
    message: ${{from_address}} is sending too fast
    action: DEFER
    count:
        sender:
            type: protocoldict
            regex: ^.*\@domain\.invalid$
            key: domain.invalid
```
