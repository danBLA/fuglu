## Summary

(Summarize the bug encountered concisely)

## fuglu version and operating system

( please provide output of `fuglu --version`)


## Steps to reproduce

(describe how the bug can be reproduced)


## What is the current bug behavior?

(What actually happens)


## What is the expected correct behavior?

(What you should see instead)


## Relevant logs and/or configuration snippets

( please provide relevant output  from `fuglu.log` / maillog / `fuglu --lint` / `fuglu_conf -n` /  ... )




/label ~bug 